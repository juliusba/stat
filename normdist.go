package stat

import (
	"math/rand"
)

type NormDist struct {
	value float64

	Mean float64
	SD   float64
	Min  float64
	Max  float64
}

func New(mean, sd, min, max float64) (nd *NormDist) {
	nd = &NormDist{
		Mean: mean,
		SD:   sd,
		Min:  min,
		Max:  max,
	}

	nd.Next()
	return
}

func (n *NormDist) Value() float64 {
	return n.value
}

func (this *NormDist) Next() float64 {
	this.value = Clampf(rand.NormFloat64()*this.SD+this.Mean, this.Min, this.Max)

	return this.value
}

func (n *NormDist) Deviation() (deviation float64) {
	return (n.value - n.Mean) / n.SD
}

func (this *NormDist) Feed(changeRate, biasConvergence float64) {
	this.Mean += (this.value - this.Mean) * changeRate
	this.SD += (this.Deviation()/(biasConvergence*0.674) - this.SD) * changeRate
}

func (this *NormDist) ReverseFeed(changeRate, biasConvergence float64) {
	this.Mean += (this.Mean - this.value) * changeRate
	this.SD += ((biasConvergence*0.674)/this.Deviation() - this.SD) * changeRate
}

func (this *NormDist) Merge(other *NormDist) *NormDist {
	newNormDist := New(this.Mean*other.Mean, this.SD*other.SD, this.Min, this.Max)
	return newNormDist
}

func (this *NormDist) MergeOnce(other *NormDist) float64 {
	value := rand.NormFloat64()*this.SD*other.SD + this.Mean*other.Mean
	if value < this.Min {
		return this.Min
	} else if value > this.Max {
		return this.Max
	}
	return value
}
