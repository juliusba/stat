package stat

import (
	"math/rand"
)

type IntNormDist struct {
	value int

	Mean float64
	SD   float64
	min  int
	max  int
}

func (this *IntNormDist) Value() int {
	return this.value
}

func (this *IntNormDist) Next() int {
	this.value = int(rand.NormFloat64()*this.SD + this.Mean + 0.5)
	if this.value > this.max {
		return this.max
	} else if this.value < this.min {
		return this.min
	}
	return this.value
}

func (this *IntNormDist) Deviation() (deviation float64) {
	return (float64(this.value) - this.Mean) / this.SD
}

func (this *IntNormDist) Feed(changeRate, biasConvergence float64) {
	this.Mean = this.Mean + (float64(this.value)-this.Mean)*changeRate
	this.SD = this.SD + (this.Deviation()/(biasConvergence*0.674)-this.SD)*changeRate
}

func (this *IntNormDist) ReverseFeed(changeRate, biasConvergence float64) {
	this.Mean = this.Mean + (this.Mean-float64(this.value))*changeRate
	this.SD = this.SD + ((biasConvergence*0.674)/this.Deviation()-this.SD)*changeRate
}

func (this *IntNormDist) Merge(other *IntNormDist) *IntNormDist {
	newNormDist := NewIntNormDist(this.Mean*other.Mean, this.SD*other.SD, this.min, this.max)
	return newNormDist
}

func NewIntNormDist(Mean, SD float64, min, max int) (ind *IntNormDist) {
	ind = &IntNormDist{
		Mean: Mean,
		SD:   SD,
		min:  min,
		max:  max,
	}

	ind.Next()
	return
}
